# Plan détaillé

## 0. Introduction
Définitions du livre et du livre numérique à travers leurs processus de conception et de fabrication. Il s'agit plus d'une tentative – ce cours est une découverte, une ouverture – que d'un cours théorique figé.

## 1. Définir ce qu'est un livre et un livre numérique
### 1.1. Trois techniques de fabrication du livre
À la main : question du temps de fabrication, limites de conception, pas d'erreur possible, et regard subjectif du copieur.

À la machine : rapidité de production, impression typographique, âge de l'artisanat du livre.

À l'ordinateur : automatisation, fabriquer avec des algorithmes (remplacements, gestion des césures et des drapeaux), âge de l'industrie du livre.

### 1.2. Quelques méthodes d'impression aujourd'hui
Sérigraphie, offset, impression numérique, POD.

Contraintes et conséquences : l'impression à la demande intègre une notion de flux (de liquidité ?) dans la chaîne de publication, qui était jusque-là assez discrète.

### 1.3. Livre numérique : de quoi parle-t-on ?
Définition officielle : longue à arriver mais très ouverte.

Origine du format EPUB : web, PWP. Utilisation hors connexion.

Limites du terme : quid des applications, des sites web et des objets numérique hybrides comme les PWA ? Test d'un livre web.


## 2. Quelques outils de fabrication de publications et de livres (papier ou numériques) contemporains
### 2.1. Traitements de texte et logiciels de PAO
Tentative de définition d'un traitement de texte : logiciel permettant d'écrire, de structurer et de mettre en forme du texte. C'est ce qu'on appelle un WYSIWYG (What you see is what you get).

Logiciel de PAO : même chose, mais pour la conception et la fabrication de fichiers destinés à l'impression.

Intérêts et problèmes/contraintes de ces outils :

- facilité d'utilisation une fois le logiciel maîtrisé ;
- on ne sait pas *vraiment* ce que l'on fait : avantage et inconvénient ;
- limites liées au logiciel : on ne peut faire que ce que nous propose le logiciel (même si InDesign peut intégrer des scripts) ;
- dépendances : .

Présenter un traitement de texte *surchargé* pour expliciter la question de l'UI.

### 2.2. Écrire avec du texte brut : quelle logique ?
Texte brut, de quoi parle-t-on ?

Quelques formats : HTML, Markdown, Asciidoc. Distinction structure et mise en forme qui permet de ne pas mettre en forme pour structurer : pérennité du texte, véritable interopérabilité, etc.

La question de la conversion ou de la génération : l'objectif n'est plus de modifier le texte pour obtenir *visuellement* ce que l'on souhaite, mais on distingue les étapes d'intervention sur le texte.

Passer du WYSIWYG au WYSIWYM (What you see is what you mean).

## 2.3. Le livre numérique pose de nouvelles questions : liens entre la fabrication (HTML) et les contraintes de lecture (interopérabilité)
Plusieurs fonctions importantes, intrinsèquement liées au livre numérique :

- interopérabilité : le livre est censé être lisible sur tous les supports, de la même façon. Pour comprendre il faut regarder comment le web fonctionne ;
- liquidité – ou notion de *responsive design* : le livre numérique est, par définition, liquide, qu'est-ce que cela veut dire ? Quelles contraintes de gestion de textes et de documents ?
- pérennité : le fichier source sera toujours lisible dans dix ans (personnellement j'ai perdu mes rares travaux étudiants du début des années 2000) ;
- multiforme : le livre numérique peut être un fichier EPUB qui sera lu dans une application qui lit des EPUBs, mais également dans une interface web (Glose, 1D touch) ou encore autre chose ;
- etc.

Donc ces avantages présentent aussi des contraintes lors de la conception et de la fabrication d'une publication ou d'un livre.

## 3. Les chaînes de publication
### 3.1. Comment gérer un texte depuis l'écriture jusqu'à l'impression ou la mise à jour d'un fichier EPUB ?
Comment trouver le moyen de *travailler* un texte, de l'éditer, à plusieurs, sur plusieurs étapes très différentes ? Un traitement de texte a par exemple plusieurs limites, mais il permet tout de même de pouvoir tenter de travailler à plusieurs de façon asynchrone.

L'intérêt ici est double :

- fluidifier et faciliter les échanges et les interventions autour d'un texte ;
- rendre toutes les étapes réversibles : retour à l'étape précédente possible sans que cela ne devienne un cauchemar.

### 3.2. Mettre en place des méthodes et des principes (gestion de projet)
Avant de parler de formats, de logiciels, de techniques, il faut surtout mettre en place des principes pour travailler à plusieurs sur un texte.


## 4. La publication numérique n'est pas qu'une question d'outils
### 4.1. Fabriquer un livre numérique, liste non exhaustive
Plusieurs outils possibles : éditeur de texte, logiciel *simple* comme Sigil, ou InDesign.

### 4.2. Quelques approches
Utiliser un logiciel, un service en ligne, *coder*, ... Le tout est de considérer les contraintes liées à ces choix : temps d'apprentissage, imperfections liées à l'outil, temps de mise en place d'une chaîne de publication, intervenants qui vont utiliser les outils (et leur courbe d'apprentissage), etc.

### 4.3. Conclusion
Apprendre à utiliser un outil c'est bien, comprendre qu'il y a plusieurs chemins pour arriver à une même destination, c'est mieux.
