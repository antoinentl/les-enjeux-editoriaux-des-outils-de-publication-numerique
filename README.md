# Les enjeux éditoriaux des outils de publication numérique
Support de présentation d'une intervention du vendredi 5 mai 2017 pour des étudiants du Master PANIST (Sciences de l'information et des bibliothèques) de l'enssib (École nationale supérieure des sciences de l'information et des bibliothèques).

Ce support de présentation est basé sur [reveal.js](https://github.com/hakimel/reveal.js).

Cette présentation et les informations associées sont sous licence CC BY-SA, sauf mention contraire.

Pour consulter ce support en ligne :

- [http://presentations.quaternum.net/les-enjeux-editoriaux-des-outils-de-publication-numerique/](http://presentations.quaternum.net/les-enjeux-editoriaux-des-outils-de-publication-numerique/)
- [version PDF](http://presentations.quaternum.net/les-enjeux-editoriaux-des-outils-de-publication-numerique/2017-05-05-antoine-fauchie-les-enjeux-editoriaux-des-outils-de-publication-numerique.pdf).

## Objectifs
Présenter une partie de l'Unité d'enseignement "Piloter un projet d'édition numérique" du Master [Publication numérique](http://www.enssib.fr/master2-publication-numerique-lyon) de l'enssib à travers une présentation de plusieurs outils et approches de publication numérique. Les objectifs sont les suivants :

1. Définir ce qu'est un livre et un livre numérique
2. Découvrir quelques outils de fabrication de livres (papier ou numériques)
3. Aborder la question des chaînes de publication
4. Désamorcer l'idée que la publication numérique n'est qu'une question d'outils

Tout cela à travers une présentation des processus de conception et de fabrication des livres et des livres numériques – ou plus globalement des documents et des publications.

Il s'agit donc d'une introduction des bouleversements engendrés par le livre numérique dans l'édition, en tentant de ne pas se focaliser sur le livre numérique en particulier : conditions de fabrication des livres, changement de paradigme outils-méthodes, notion de liquidité, etc.


## Cadre et dispositifs
Trois heures d'intervention devant une vingtaine d'étudiants, avec un vidéoprojecteur et un tableau.

Présenter, à partir d'une même source, une expérimentation de génération multiple d'un document : web, EPUB, HTML et papier. Deux exemples : *Resilient Web Design* de Jeremy Keith, et "Hinterland" de Frank Adebiaye.


## Plan
Proposition de plan ci-dessous, plan détaillé [disponible ici](/plan-detaille.md).

### 1. Définir ce qu'est un livre et un livre numérique
#### 1.1. Trois techniques de fabrication du livre
À la main, à la machine, à l'ordinateur.
#### 1.2. Quelques méthodes d'impression aujourd'hui
Sérigraphie, offset, impression numérique, POD. Contraintes et conséquences.
#### 1.3. Livre numérique : de quoi parle-t-on ?
Définition, origine du format (web, PWP), limites du terme (applications, sites web, PWA).

### 2. Quelques outils de fabrication de publications et de livres (papier ou numériques) contemporains
#### 2.1. Traitements de texte et logiciels de PAO
Intérêts et problèmes/contraintes de ces outils : facilité d'utilisation une fois le logiciel maîtrisé, limites liées au logiciel, dépendances.
#### 2.2. Écrire avec du texte brut : quelle logique ?
Texte brut, de quoi parle-t-on ? Quelques formats : HTML, Markdown, Asciidoc. La question de la conversion ou de la génération.
### 2.3. Le livre numérique pose de nouvelles questions : liens entre la fabrication (HTML) et les contraintes de lecture (interopérabilité)
Le livre numérique est, par définition, liquide, qu'est-ce que cela veut dire ? Quelles contraintes de gestion de textes et de documents ?

### 3. Les chaînes de publication
#### 3.1. Comment gérer un texte depuis l'écriture jusqu'à l'impression ou la mise à jour d'un fichier EPUB ?
Comment trouver le moyen de *travailler* un texte, de l'éditer, à plusieurs, sur plusieurs étapes très différentes ? Pourquoi ? Quelle différence avec
#### 3.2. Mettre en place des méthodes et des principes (gestion de projet)

### 4. La publication numérique n'est pas qu'une question d'outils
#### 4.1. Fabriquer un livre numérique, liste non exhaustive
Plusieurs outils possibles : éditeur de texte, logiciel *simple* comme Sigil, ou InDesign.
#### 4.2. Quelques approches
Utiliser un logiciel, *coder*, ...
#### 4.3. Conclusion
